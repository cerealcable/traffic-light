traffic light
=============

A small application that manages a Traffic Light connected Raspberry Pi. The project is split in two, a frontend and backend.


## Frontend
An Angular based frontend client allowing easy light management.

## Backend
A Flask based backend that uses flask-restx for a well defined API. See /api to see some swagger docs!

## Deployment
Deployment files and tools.
