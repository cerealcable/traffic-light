traffic light
=============

Tiny Flask application that lets me change a traffic light I have wired up to relays on my Raspberry Pi. Just a rest api for now!

Requirements:
```
sudo apt install python3-pip python3-rpi.gpio
pip install -r requirements.txt
```

Running:
```
python3 traffic_flask.py
```
