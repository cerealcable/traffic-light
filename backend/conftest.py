# python

# packages
import pytest

# local
from traffic_light import app as flask_app


@pytest.fixture
def app():
    yield flask_app
