import pytest


def test_get(client):
    response = client.get("/api/light")
    assert response.status_code == 200
    assert all(param in response.json for param in ['red', 'yellow', 'green'])


def test_post(client):
    response = client.post("/api/light", json={
        'red': "on",
        'yellow': "on",
        'green': "on",
    }, )
    assert response.status_code == 200
