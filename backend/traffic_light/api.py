# python

# packages
from flask import Blueprint, request
from flask_restx import Resource, Api, fields

# local
from .traffic_light import TrafficLight


tl = TrafficLight()
blueprint = Blueprint('api', __name__)
api = Api(blueprint)
ns = api.namespace('', description='Light Operations')
model_light = api.model('Light', {
    'red': fields.String(description='State of red light', enum=['on', 'off']),
    'yellow': fields.String(description='State of yellow light', enum=['on', 'off']),
    'green': fields.String(description='State of green light', enum=['on', 'off']),
})


@ns.route('/light')
class Lights(Resource):
    @ns.marshal_list_with(model_light)
    def get(self):
        return {
            "red": "on" if tl.get_light("red") else "off",
            "yellow": "on" if tl.get_light("yellow") else "off",
            "green": "on" if tl.get_light("green") else "off",
        }

    @ns.expect(model_light)
    @ns.marshal_with(model_light)
    def post(self):
        body = request.get_json()
        for color, state in body.items():
            newState = None
            if state.lower() == "on":
                newState = True
            elif state.lower() == "off":
                newState = False
            if newState is not None:
                tl.set_light(color, newState)
        return {
            "red": "on" if tl.get_light("red") else "off",
            "yellow": "on" if tl.get_light("yellow") else "off",
            "green": "on" if tl.get_light("green") else "off",
        }
