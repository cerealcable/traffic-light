# python

# packages
from flask import Flask
from flask_cors import CORS

# local
from .api import blueprint as api

app = Flask(__name__)
app.register_blueprint(api, url_prefix='/api')
CORS(app)
