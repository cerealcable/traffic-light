try:
    import RPi.GPIO as GPIO
except:
    import Mock.GPIO as GPIO


class TrafficLight:
    RELAY_1_PIN = 37
    RELAY_2_PIN = 38
    RELAY_3_PIN = 40

    LIGHTS = {
        "red": RELAY_1_PIN,
        "yellow": RELAY_2_PIN,
        "green": RELAY_3_PIN,
    }
    LIGHT_VALUES = {
        True: GPIO.LOW,
        False: GPIO.HIGH,
    }
    LIGHT_STATE = {
        "red": False,
        "yellow": False,
        "green": False
    }

    def __init__(self):
        print("TrafficLight GPIO Setup")
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.RELAY_1_PIN, GPIO.OUT)
        GPIO.setup(self.RELAY_2_PIN, GPIO.OUT)
        GPIO.setup(self.RELAY_3_PIN, GPIO.OUT)
        print("TrafficLight GPIO Initial States")
        GPIO.output(self.RELAY_1_PIN, GPIO.HIGH)
        GPIO.output(self.RELAY_2_PIN, GPIO.HIGH)
        GPIO.output(self.RELAY_3_PIN, GPIO.HIGH)

    def set_light(self, color, state):
        _pin = self.LIGHTS.get(color, None)
        if not _pin:
            raise RuntimeError("Invalid color")
        GPIO.output(_pin, self.LIGHT_VALUES[state])
        self.LIGHT_STATE[color] = state

    def get_light(self, color):
        return self.LIGHT_STATE.get(color)


def main():
    tl = TrafficLight()
    tl.set_light("red", True)
    tl.set_light("yellow", True)
    tl.set_light("green", True)


if __name__ == "__main__":
    main()
