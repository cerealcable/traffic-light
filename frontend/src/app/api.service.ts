import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface LightState {
  red: string,
  yellow: string,
  green: string
}

@Injectable()
export class ApiService {
  lightState: LightState = {
    red: 'off',
    yellow: 'off',
    green: 'off'
  }

  constructor(private http: HttpClient) {
    console.log('ApiService init')
    this.http.get('/api/light').subscribe((data: LightState) => {
      console.log(`ApiService init retrieved ${JSON.stringify(data)}`);
      this.lightState = data;
    });
  }

  getState(): LightState {
    console.log('ApiService.getState()');
    return this.lightState;
  }

  setState(newLightState: LightState) {
    console.log('ApiService.setState()');
    this.http.post<LightState>('/api/light', newLightState).subscribe((data: LightState) => {
      console.log(`Updated State: ${JSON.stringify(data)}`);
      this.lightState = data;
    })
  }
}
