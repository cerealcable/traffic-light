import { Component, HostListener } from '@angular/core';
import { ApiService, LightState } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'traffic-light';
  apiService: ApiService;

  constructor(apiService: ApiService) {
    console.log('AppComponent init');
    this.apiService = apiService;
  }

  @HostListener('document:keydown.o')
  @HostListener('document:keydown.0')
  onKeypressOffEvent() {
    console.log('AppComponent.onKeypressOffEvent()');
    let newState: LightState = this.apiService.getState();
    newState.red = "off"
    newState.yellow = "off"
    newState.green = "off"
    this.apiService.setState(newState)
  }

  @HostListener('document:keydown.r')
  @HostListener('document:keydown.1')
  onKeypressRedEvent() {
    console.log('AppComponent.onKeypressRedEvent()');
    let newState: LightState = this.apiService.getState();
    newState.red = "on"
    newState.yellow = "off"
    newState.green = "off"
    this.apiService.setState(newState)
  }

  @HostListener('document:keydown.y')
  @HostListener('document:keydown.2')
  onKeypressYellowEvent() {
    console.log('AppComponent.onKeypressYellowEvent()');
    let newState: LightState = this.apiService.getState();
    newState.red = "off"
    newState.yellow = "on"
    newState.green = "off"
    this.apiService.setState(newState)
  }

  @HostListener('document:keydown.g')
  @HostListener('document:keydown.3')
  onKeypressGreenEvent() {
    console.log('AppComponent.onKeypressGreenEvent()');
    let newState: LightState = this.apiService.getState();
    newState.red = "off"
    newState.yellow = "off"
    newState.green = "on"
    this.apiService.setState(newState)
  }

  toggleLight(color: string) {
    console.log(`Toggling ${color}`);
    let newState: LightState = this.apiService.getState();
    switch(color) {
      case "red": {
        if(newState.red === "on") {
          newState.red = "off";
        } else {
          newState.red = "on";
        }
        this.apiService.setState(newState);
        break;
      }
      case "yellow": {
        if(newState.yellow === "on") {
          newState.yellow = "off";
        } else {
          newState.yellow = "on";
        }
        this.apiService.setState(newState);
        break;
      }
      case "green": {
        if(newState.green === "on") {
          newState.green = "off";
        } else {
          newState.green = "on";
        }
        this.apiService.setState(newState);
        break;
      }
      default: {
        console.log("Unexpected color for toggleLight");
      }
    }
  }
}
